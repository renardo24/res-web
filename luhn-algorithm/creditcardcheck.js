
    /* Credit Card Checker - (c)oded '05 shaman, www.nox.org.uk                                                   *
     * Function to check for valid credit card numbers using the LUHN algorithm.                                  *
     * This code is based on the wikicode pseudocode at: http://en.wikipedia.org/wiki/Luhn_algorithm              *
     * You can see a working example of this script here: http://www.nox.org.uk/code/examples/creditcardcheck.php *
     * Do what you want with this code, but please give credit where possible.                                    */

    function validatecc(x) {
      var ccnumber=x.value.replace(/\D/g, '');
      var cclength=ccnumber.length;
      var parity=cclength % 2;
      var sum=0;
      for (i=0; i < cclength; i++) {
        var ccdigit=ccnumber.charAt(i);
        if (i % 2 == parity) ccdigit=ccdigit * 2;
        if (ccdigit > 9) ccdigit=ccdigit - 9;
        sum = sum + parseInt(ccdigit);
      }
      var valid=(sum % 10 == 0);
      if (valid) alert("Valid"); else alert("Not Valid");
    }

