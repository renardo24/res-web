SMART Goals

S.M.A.R.T. GOALS are:
  - Specific
    - Well defined
    - Clear to anyone that has a basic knowledge of the project
    - Define precisely the objective or outcome you want. Objective or goal
      can't be diffuse or nebulous but should be precisely defined.
    - Provide enough detail so that there is no indecision as to what exactly
      you should be doing when the time comes to do it. A goal of: "Study
      biology" is poor. Should you be reading your text? If so, what pages?
      Or should you be looking over your lecture notes? A much better goal
      would be: "Read pp. 12 - 35 in biology text, write questions in the
      margin of text, and practice answering those questions after reading."

  - Measurable
    - Know if the goal is obtainable and how far away completion is
    - Know when it has been achieved
    - Define objectively how you will know when you've attained it. Define a
      method of measuring the objective/goal.
    - Your goal should be such that when you are through you have some
      tangible evidence of completion. It feels good to see something there
      in front of you indicating a job well done. Equally important, you
      will be able to prove to yourself that you were successful and your
      time wasn't wasted. The end result of a goal such as "Read Chapter 3"
      cannot be reliably assessed. Did you fully understand the words when
      you looked at the pages? A much better goal would be: "Read Chapter 3
      and then write a summary from memory." The summary would indicate that
      you in fact did read the chapter and would allow you to evaluate your
      degree of understanding. Producing tangible evidence requires ACTIVE
      studying on your part, which research clearly suggests will produce
      superior learning and retention.

  - Agreed-To/Achievable/Action-oriented/Acceptable
    - Agreement with all the stakeholders what the goals should be
    - Use action verbs to describe the steps required. All parties need to
      agree to the objective/goal, and it also must be achievable.
    - Your goal should be set by you rather than by someone else. You know
      best your strengths and weaknesses, and can use this information to
      maximize your chances of success.

  - Realistic/Rewarding
    - Within the availability of resources, knowledge and time
    - Confirm your belief that the goal is indeed possible. It must be a
      realistic objective/goal, and it must make sense to do it.
    - Don't plan to do things if you are unlikely to follow through. Better
      to plan only a few things and be successful rather than many things
      and be unsuccessful. Success breeds success! Start small, with what
      you can do, experience the joys of meeting your goal, and only then
      gradually increase the amount of work that you ask of yourself.
      Setting goals in which every minute in the day is accounted for is
      unrealistic; unplanned events will crop up and wreak havoc with your
      schedule. Give yourself some flexibility.

  - Timely
    - Enough time to achieve the goal
    - Not too much time, which can affect project performance
    - Set a deadline for reaching your goal. What is your ETA (Estimated
      Time of Arrival)? Without a schedule and due date, it will just keep
      going, and going, and going.
    - Say when you plan to work at your goal, e.g., between 4:00 - 5:00 p.m.
      Anything that will take you more that two hours to complete, break
      into smaller, more manageable chunks.


To these I would add:
  - Consistent -- Your goal must not conflict with another goal, and must be
    congruent with each and every of your values, purpose, vision and
    mission. Otherwise difficulty will ensue.

  - Humane -- Your goal must not do another person harm. Don't forget about
    being kind, caring, and compassionate. The end does not justify the
    means.

So, that would change the acronym to 'SCHMART'. (Okay, it's a stretch I
admit, but it works.)

Goals are achieved only through action. Goal achievement is ensured via the
repeating cycle of the three action elements:
  1)  Action -- You've planned your work, now work your plan. The
      unambiguous 'who, what, when, where, how, and how much' definitions of
      specific tasks. Physical activity starts the process moving; and be
      sure to complete each small step you're taking.

  2)  Review Results -- On specific dates, monitor progress by comparing
      your plan with actual measured results. Don't deceive yourself. Check
      expected results against what's really real.

  3)  Revise Tactics -- Adapt when change is indicated. Don't revise the
      goal yet; change only the means to achieving the goal -- the tactics.
      Determine what works and what doesn't. Implement the revised plan
      (starts the cycle again).

--------------------

Personal goals
  - Individuals can have personal goals. A student may set a goal of a high
    mark in an exam. An athlete might walk five miles a day. A traveler
    might try to reach his destination city within three hours.

  - Managing goals can give returns in all areas of life. By knowing
    precisely what one wants to achieve, makes clear what to concentrate and
    improve on.

  - Goal setting and planning (goalwork) gives long-term vision and
    short-term motivation. It focuses acquisition of knowledge and helps to
    organize resources.

Efficient Goalwork includes recognizing and resolving any guilt, inner
conflict or limiting belief that might cause you to sabotage your efforts.
By setting clearly defined goals, one can measure and take pride in the
achievement of those goals. One can see progress in what might have seemed a
long grind.


Achieving personal goals
  - Achieving complex and difficult goals requires focus, long-term
    diligence and effort. Success in any field will require foregoing
    blaming, excuses and justifications for poor performance or lack of
    adequate planning or in short emotional maturity.

  - Long term achievements are based on short-term achievements. Emotional
    control over the small moments of the single day makes a big difference
    in the long term.

  - By accepting a degree of realism within one's own goals, one allows
    oneself not to change reality to match his own dreams by his own efforts
    alone, but to accept it how it is until a certain degree. This degree of
    "laziness" can prevent one from falling in unhappiness by losing too
    much control of life by trying to specialize in a very small area and to
    become a top leader in that field. No matter what level of society one
    belongs to, it is very likely that there are levels above and below.

--------------------

Goal setting
  - Goal Setting involves setting specific, measurable and time targeted
    objectives. In an organizational or business context, it may be an
    effective tool for making progress by ensuring that participants are
    clearly aware of what is expected from them, if an objective is to be
    achieved. On a personal level, Goal setting is a process that allows
    people to specify then work towards their own objectives - most commonly
    with financial or career-based goals. Goal setting is a major component
    of Personal development literature.

  - The business technique of Management by objectives uses the principle of
    goal setting. In business, goal setting has the advantages of
    encouraging participants to put in substantial effort; and, because
    every member is aware of what is expected of him or her (high role
    perception), little room is left for inadequate effort going unnoticed.

  - To be most effective goals should be tangible, specific, realistic and
    have a time targeted for completion. There must be realistic plans to
    achieve the intended goal. For example, setting a goal to go to Mars on
    a shoe string budget is not a realistic goal while setting a goal to go
    to Hawaii as a backpacker is a possible goal with possible, realistic
    plans.

  - One drawback of goal setting is that implicit learning may be inhibited.
    This is because goal setting may encourage simple focus on an outcome
    without openness to exploration, understanding or growth. "Goals provide
    a sense of direction and purpose" (Goldstein, 1993, p.96). Locke et al.
    (1981) examined the behavioral effects of goal-setting, concluding that
    90% of laboratory and field studies involving specific and challenging
    goals led to higher performance than easy or no goals.

--------------------

How to Write SMART Goals
  - Like clearing out the attic or weeding the garden, the more chaos and
    mess there is, the less you want to do it! The only instruction I can
    give is to stop procrastinating and just do it!

  - You?ll be glad you did! People?s main concern is often that the goals
    they had written were not ?right?. There is no such thing as a ?right?
    goal, only what has meaning for you.

  - A goal that is written 70% well is better than no goal at all. Give
    yourself permission to give it a try, and worry about fine-tuning your
    goals later.

  - Top Tip - You should limit your goals to between 5 and 7 at any one
    time. To achieve your goals you have to focus your efforts and
    attention. As you progress and complete goals, you may add new goals. If
    you find yourself with more than 7 goals, you are probably writing
    mini-goals or tasks. Remember to keep your goals focussed on a major
    area of responsibility.

--------------------

Resources:
- http://www.crazycolour.com/os/goalsetting_02.shtml
- http://en.wikipedia.org/wiki/Objective_(goal)
- http://en.wikipedia.org/wiki/SMART_%28project_management%29
- http://en.wikipedia.org/wiki/Goal_%28management%29
- http://www.uncommon-knowledge.co.uk/goal_setting/goals.html
- http://www.uncommon-knowledge.co.uk/goal_setting/smart_4.html
- http://www.goal-setting-guide.com/smart-goals.html
- http://www.coun.uvic.ca/learn/program/hndouts/smartgoals.html
- http://www.projectsmart.co.uk/smart_goals.html
