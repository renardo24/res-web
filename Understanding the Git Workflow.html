<!DOCTYPE html>
<!-- saved from url=(0044)https://sandofsky.com/blog/git-workflow.html -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" class="fa-events-icons-ready wf-fftisaweb-n4-active wf-fftisawebbold-n4-active wf-fftisawebitalic-n4-active wf-fftisawebmediumsmallcaps-n4-active wf-fftisawebsmallcaps-n4-active wf-nitti-n4-active wf-museosans300-n4-active wf-museosans100-n4-active wf-icon-n4-inactive wf-active"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="openid.server" href="http://www.myopenid.com/server">
    <link rel="openid.delegate" href="http://sandofsky.myopenid.com/">
	<link href="./Understanding the Git Workflow_files/normalize.css" media="screen,print" rel="stylesheet" type="text/css">
    <link href="./Understanding the Git Workflow_files/main.css" media="screen,print" rel="stylesheet" type="text/css">
    <link href="./Understanding the Git Workflow_files/code.css" media="screen,print" rel="stylesheet" type="text/css">
    <link href="http://feeds2.feedburner.com/SandofskyBlogEntries" rel="alternate" title="RSS Feed" type="application/atom+xml">
	<link rel="canonical" href="https://sandofsky.com/blog/git-workflow.html">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>Understanding the Git&nbsp;Workflow</title>
    <meta name="description" content="Benjamin Sandofsky, a Software Engineer in San Francisco, California.">
    <meta name="keywords" content="ben sandofsky,benjamin sandofsky,twitter,twitter developer,systems programming,scaling,software engineering,iphone development,rails,ruby on rails,ruby on rails development,objective-c,short films,films,filmaking,comedy,voice mail,shoes,tequila,improv,improv comedy,writer,writing,actor,improviser">
    <meta property="twitter:account_id" content="699463">
  <link rel="stylesheet" href="./Understanding the Git Workflow_files/embedded_fonts_cae15a3f56e8e9a6b01daef14c3fe5dcb626b57f.css" media="all"><link rel="stylesheet" href="./Understanding the Git Workflow_files/counters.css" media="all"><link href="./Understanding the Git Workflow_files/840a321d95.css" media="all" rel="stylesheet"></head>
  <body style="visibility: visible;">
	<nav><a href="https://sandofsky.com/" class="site-title"><span class="section"><span class="semi-collapseable">Ben<span class="collapseable">jamin</span> </span>Sandofsky</span></a><a href="https://sandofsky.com/writing.html" class="blog"><span class="section">Writing</span></a><a href="https://sandofsky.com/projects.html" class="projects"><span class="section">Projects</span></a><a href="https://twitter.com/sandofsky" class="projects"><span class="section last">Twitter</span></a>
	</nav>
	<div class="longform blog">
	<div itemscope="" itemtype="http://schema.org/Article">
	  <h1 itemprop="name">Understanding the Git&nbsp;Workflow</h1>
	  <div itemprop="articleBody" class="article-body content">
		  <p>If you don’t understand the motivation behind Git’s design, you’re in for a world of hurt. With enough flags you can force Git to act the way you think it should instead of the way it wants to. But that’s like using a screwdriver like a hammer; it gets the job done, but it’s done poorly, takes longer, and damages the screwdriver.</p>

<p>Consider how a common Git workflow falls apart.</p>

<ol>
  <li><em>Create a branch off Master</em></li>
  <li><em>Work</em></li>
  <li><em>Merge it back to Master when done</em></li>
</ol>

<p>Most of the time this behaves as you expect because Master changed since you branched. Then one day you merge a feature branch into Master, but Master hasn’t diverged. Instead of creating a merge commit, Git points Master to the latest commit on the feature branch, or “fast forwards.” (<a href="https://sandofsky.com/images/fast_forward.pdf">Diagram</a>)</p>

<p>Unfortunately, your feature branch contained checkpoint commits, frequent commits that back up your work but captures the code in an unstable state. Now these commits are indistinguishable from Master’s stable commits. You could easily roll back into a disaster.</p>

<p>So you add a new rule: “When you merge in your feature branch, use <em>–no-ff</em> to force a new commit.” This gets the job done, and you move on.</p>

<p>Then one day you discover a critical bug in production, and you need to track down when it was introduced. You run <a href="http://book.git-scm.com/5_finding_issues_-_git_bisect.html">bisect</a> but keep landing on checkpoint commits. You give up and investigate by hand.</p>

<p>You narrow the bug to a single file. You run <code>blame</code> to see how it changed in the last 48 hours. You know it’s impossible, but <code>blame</code> reports the file hasn’t been touched in weeks. It turns out <code>blame</code> reports changes for the time of the initial commit, not when merged. Your first checkpoint commit modified this file weeks ago, but the change was merged in today.</p>

<p>The <code>no-ff</code> band-aid, broken <code>bisect</code>, and <code>blame</code> mysteries are all symptoms that you’re using a screwdriver as a hammer.</p>

<h3 id="rethinking-revision-control">Rethinking Revision Control</h3>

<p>Revision control exists for two reasons.</p>

<p>The first is to help the act of writing code. You need to sync changes with teammates, and regularly  back up your work. Emailing zip files doesn’t scale.</p>

<p>The second reason is <a href="http://en.wikipedia.org/wiki/Software_configuration_management">configuration management</a>. This includes managing parallel lines of development, such as working on the next major version while applying the occasional bug fix to the existing version in production. Configuration management is also used to figure out when exactly something changed, an invaluable tool in diagnosing bugs.</p>

<p>Traditionally, these two reasons conflict.</p>

<p>When prototyping a feature, you should make regular checkpoint commits. However, these commits usually break the build.</p>

<p>In a perfect world, every change in your revision history is succinct and stable. There are no checkpoint commits that create line noise. There are no giant, 10,000 line commits. A clean history makes it easy to revert changes or <a href="http://gitready.com/intermediate/2009/03/04/pick-out-individual-commits.html">cherry-pick</a> them between branches. A clean history is easy to later inspect and analyze. However, maintaining a clean history would mean waiting to check in changes until they’re perfect.</p>

<p>So which approach do you choose? Regular commits, or a clean history?</p>

<p>If you’re hacking on a two man pre-launch startup, clean history buys you little. You can get away with committing everything to Master, and deploying whenever you feel like it.</p>

<p>As the consequences of change increase, be it a growing development team or the size of your user base, you need tools and techniques to keep things in check. This includes automated tests, code review, and a clean history.</p>

<p>Feature branches seem like a happy middle ground. They solve the basic problems of parallel development. You’re thinking of integration at the least important time, when you’re writing the code, but it will get you by for some time.</p>

<p>When your project scales large enough, the simple branch/commit/merge workflow falls apart. The time for duct-tape is over. You need a clean revision history.</p>

<p>Git is revolutionary because it gives you the best of both worlds. You can regularly check in changes while prototyping a solution but deliver a clean history when you’re finished. When this is your goal, Git’s defaults make a lot more sense.</p>

<h2 id="the-workflow">The Workflow</h2>

<p>Think of branches in two categories: public and private.</p>

<p>Public branches are the authoritative history of the project. In a public branch, every commit should be succinct, atomic, and have a well documented commit message. It should be as linear as possible. It should be immutable. Public branches include Master and release branches.</p>

<p>A private branch is for yourself. It’s your scratch paper while working out a problem.</p>

<p>It’s safest to keep private branches local. If you do need to push one, maybe to synchronize your work and home computers, tell your teammates that the branch you pushed is private so they don’t base work off of it.</p>

<p>You should never merge a private branch directly into a public branch with a vanilla <em>merge</em>. First, clean up your branch with tools like reset, rebase, squash merges, and commit amending.</p>

<p>Treat yourself as a writer and approach each commit as a chapter in a book. Writers don’t publish first drafts. Michael Crichton said, “Great books aren’t written– they’re rewritten.”</p>

<p>If you come from other systems, modifying history feels taboo. You’re conditioned that anything committed is written in stone. By that logic we should disable “undo” in our text editors.</p>

<p>Pragmatists care about changes until the changes become noise. For configuration management, we care about big-picture changes. Checkpoint commits are just a cloud-backed undo buffer.</p>

<p>If you treat your public history as pristine, fast-forward merges are not only safe but preferable. They keep revision history linear and easier to follow.</p>

<p>The only remaining argument for <em>–no-ff</em> is “documentation.” People may use merge commits to represent the last deployed version of production code. That’s an antipattern. Use tags.</p>

<h2 id="guidelines-and-examples">Guidelines and Examples</h2>

<p>I use three basic approaches depending on the size of my change, how long I’ve been working on it, and how far the branch has diverged.</p>

<h3 id="short-lived-work">Short lived work</h3>

<p>The vast majority of the time, my cleanup is just a squash merge.</p>

<p>Imagine I create a feature branch and create a series of checkpoint commits over the next hour:</p>

<figure class="highlight"><pre><code class="language-bash" data-lang="bash">git checkout -b private_feature_branch
touch file1.txt
git add file1.txt
git commit -am <span class="s2">"WIP"</span></code></pre></figure>

<p>When I’m done, instead of a vanilla <em>git merge</em>, I’ll run:</p>

<figure class="highlight"><pre><code class="language-bash" data-lang="bash">git checkout master
git merge --squash private_feature_branch
git commit -v</code></pre></figure>

<p>Then I spend a minute writing a detailed commit message.</p>

<h3 id="larger-work">Larger work</h3>

<p>Sometimes a feature sprawls into a multi-day project, with dozens of small commits.</p>

<p>I decide my change should be broken into smaller changes, so squash is too blunt an instrument. (As a rule of thumb I ask, “Would this be easy to code review?”)</p>

<p>If my checkpoint commits followed a logical progression, I can use <a href="http://kernel.org/pub/software/scm/git/docs/git-rebase.html">rebase</a>’s Interactive  Mode.</p>

<p>Interactive mode is powerful. You can use it to edit an old commits, split them up, reorder them, and in this case squash a few.</p>

<p>On my feature branch:</p>

<figure class="highlight"><pre><code class="language-bash" data-lang="bash">git rebase --interactive master</code></pre></figure>

<p>It then opens an editor with a list of commits. On each line is the operation to perform, the SHA1 of the commmit, and the current commit message. A legend is provided with a list of possible commands.</p>

<p>By default, each commit uses “pick,” which doesn’t modify the commit.</p>

<figure class="highlight"><pre><code class="language-bash" data-lang="bash">pick ccd6e62 Work on back button
pick 1c83feb Bug fixes
pick f9d0c33 Start work on toolbar</code></pre></figure>

<p>I change the operation to “squash,” which squashes the second commit into the first.</p>

<figure class="highlight"><pre><code class="language-bash" data-lang="bash">pick ccd6e62 Work on back button
squash 1c83feb Bug fixes
pick f9d0c33 Start work on toolbar</code></pre></figure>

<p>When I save and close, a new editor prompts me for the commit message of the combined commit, and then I’m done.</p>

<h3 id="declaring-branch-bankruptcy">Declaring Branch Bankruptcy</h3>

<p>Maybe my feature branch existed for a very long time, and I had to merge several branches into my feature branch to keep it up to date while I work. History is convoluted. It’s easiest to grab the raw diff create a clean branch.</p>

<figure class="highlight"><pre><code class="language-bash" data-lang="bash">git checkout master
git checkout -b cleaned_up_branch
git merge --squash private_feature_branch
git reset</code></pre></figure>

<p>I now have a working directory full of my changes and none of the baggage from the previous branch. Now I manually add and commit my changes.</p>

<h2 id="summary">Summary</h2>

<p>If you’re fighting Git’s defaults, ask why.</p>

<p>Treat public history as immutable, atomic, and easy to follow. Treat private history as disposable and malleable.</p>

<p>The intended workflow is:</p>

<ol>
  <li>Create a private branch off a public branch.</li>
  <li>Regularly commit your work to this private branch.</li>
  <li>Once your code is perfect, clean up its history.</li>
  <li>Merge the cleaned-up branch back into the public branch.</li>
</ol>

<p><em>Special thanks to @<a href="http://twitter.com/joshaber">joshaber</a> and @<a href="http://twitter.com/jbarnette">jbarnette</a> for providing feedback on an early draft.</em></p>

	  </div>
	  <div class="signature content">
	<div class="photo-cell"><img src="./Understanding the Git Workflow_files/headshot_1x.jpg" srcset="/images/headshot_1x.jpg 1x, /images/headshot_2x.jpg 2x, /images/headshot_3x.jpg 3x" class="headshot"></div>
	<div class="bio-cell">
		<p>I'm Ben Sandofsky. I build iOS apps, most recently <a href="https://halide.cam/download">Halide</a>. Sometimes I advise mobile startups and teach. Follow me on Twitter at&nbsp;<a href="http://twitter.com/sandofsky"><span class="twitter-at">@</span>sandofsky</a>.</p>
	</div>
</div>

	</div>
</div>

<div class="page-footer">
	<div class="footer-content">
		<div class="page-footer-column">
			<div class="around-the-web">
	<h2>Around the Web</h2>
	<ul class="social">
		<li><a href="mailto:ben@sandofsky.com"><span class="icon-font"></span> Email</a></li>
		<li><a href="https://twitter.com/sandofsky"><span class="icon-font"></span> Twitter</a></li>
		<li><a href="https://sandofsky.com/blog/git-workflow.html#"></a><a href="http://feeds2.feedburner.com/SandofskyBlogEntries"><span class="icon-font"></span> RSS</a></li>
	</ul>
	<ul class="social">
		<li><a href="https://sandofsky.com/blog/git-workflow.html#"></a><a href="https://github.com/sandofsky"><span class="icon-font"></span> GitHub</a></li>
		<li><a href="https://sandofsky.com/blog/git-workflow.html#"></a><a href="https://medium.com/@sandofsky"><span class="icon-font"></span> Medium</a></li>
		<li><a href="https://instagram.com/sandofsky"><span class="icon-font"></span> Instagram</a></li>
	</ul>
</div>
		</div>
		<div class="page-footer-column">
			<h2>Recent Writing</h2>
			<ul class="writing-preview">
			  
				    <li><a href="https://sandofsky.com/blog/controller-hierarchies.html" class="title">Controller Hierarchies</a>Occasionally an activity within your app requires multiple screens. Consider uploading a photo to Instagram: pick a photo, pick the filter, and add a description. When the user taps a photo in the first screen, it might look like:

 <a href="https://sandofsky.com/blog/controller-hierarchies.html">→</a></li>
			  
				    <li><a href="https://sandofsky.com/blog/how-to-learn-ios-development.html" class="title">How I'd learn iOS Development</a>From time to time, people ask me how to learn iOS development. So here we go.

 <a href="https://sandofsky.com/blog/how-to-learn-ios-development.html">→</a></li>
			  
				    <li><a href="https://sandofsky.com/blog/manager-classes.html" class="title">The Trouble with Manager Objects</a>The first time I look at a legacy project, I scan for warning signs. I’ve covered <a href="https://sandofsky.com/blog/singletons.html">singletons</a>, excessive <a href="https://sandofsky.com/blog/delegates-vs-observers.html">observers</a>, and today I’ll talk about “Manager” classes.

 <a href="https://sandofsky.com/blog/manager-classes.html">→</a></li>
			  
			</ul>
		</div>
		<div class="page-footer-column">
		</div>
	</div>
</div>

<script type="text/javascript" async="" src="./Understanding the Git Workflow_files/linkid.js"></script><script async="" src="./Understanding the Git Workflow_files/analytics.js"></script><script src="./Understanding the Git Workflow_files/webfont.1.5.18.js"></script><script type="text/javascript">
	document.getElementsByTagName("body")[0].style.visibility = "hidden"
	setTimeout(function(){
		var e = document.getElementsByTagName("body")[0]
		e.style.visibility = "visible"
	}, 1000)
</script>
<script>
	WebFontConfig = {
		custom: {
		    families: ["FFTisaWeb",
			"FFTisaWebBold",
			"FFTisaWebItalic",
			"FFTisaWebMediumSmallCaps",
			"FFTisaWebSmallCaps", "Nitti", "MuseoSans-300", "MuseoSans-100", "icon"],
		    urls: ['/cacheforever/embedded_fonts_cae15a3f56e8e9a6b01daef14c3fe5dcb626b57f.css', '/fonts/counters.css']
		}
	};
	   (function(d) {
	      var wf = d.createElement('script'), s = d.scripts[0];
	      wf.src = '/javascripts/webfont.1.5.18.js';
	      s.parentNode.insertBefore(wf, s);
	   })(document);
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2226333-2', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');

</script>
  
</body></html>