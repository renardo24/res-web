function setStyle(style)
{
	if (document.getElementsByTagName) {
		var styles = document.getElementsByTagName('link');
		var n = 0;
		
		for (i = 0; i < styles.length; i++)
		{
			var title = styles[i].getAttribute("title");
			if (title && title != style) {
				styles[i].disabled = true;
			} else if (title && title == style){
				styles[i].disabled = false;
				n = 1;
			}
		}
		// ensure that at least one style is enabled
		if ( n == 0) styles[0].disabled = false;
		     
		var time = new Date();
		time.setTime(time.getTime() + 31536000000);
		document.cookie = 'gr_style=' + escape(style) + '; expires=' + time.toGMTString() + '; path=/';
	}
}

function setup() {
	var cooki = document.cookie.split(";");
	var name = "gr_style=";
	var styles = "";

	for(i = 0; i < cooki.length; i++) {
    	var c = cooki[i];
       
    	while(c.charAt(0) == " ") c = c.substring(1,c.length);
    	
	  	if (c.indexOf(name) == 0) styles = c.substring(name.length, c.length); 
 	}
 	
 	if (styles == "") { styles = "cyan"; }
	
	setStyle('red');
	setStyle(styles);
}

document.onload = setup();

/*
     FILE ARCHIVED ON 11:30:33 Jun 16, 2006 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 18:11:32 Mar 18, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 34.311 (3)
  esindex: 0.006
  captures_list: 50.927
  CDXLines.iter: 10.669 (3)
  PetaboxLoader3.datanode: 57.485 (4)
  exclusion.robots: 0.142
  exclusion.robots.policy: 0.132
  RedisCDXSource: 3.02
  PetaboxLoader3.resolve: 30.32
  load_resource: 78.533
*/